vim.loader.enable()
require("chmou.settings")

--- Install pack
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	{
		"isobit/vim-caddyfile",
		event = "VeryLazy",
	},
	{ "lambdalisue/suda.vim", event = "VeryLazy" },
	{ "hashivim/vim-terraform", event = "VeryLazy" },
	{
		"ethanholz/nvim-lastplace",
		event = "VeryLazy",
		config = true,
	},
	{
		"nvimdev/lspsaga.nvim",
		event = "BufRead",
		keys = {
			{ "gj", "<cmd>Lspsaga peek_definition<cr>", desc = "Peek Definition" },
			{ "]e", "<cmd>Lspsaga diagnostic_jump_next<cr>", desc = "Peek Next Definition" },
			{ "[e", "<cmd>Lspsaga diagnostic_jump_prev<cr>", desc = "Peek Prev Definition" },
		},
		opts = {
			-- ui = { code_action = '' },
			lightbulb = { enable = false, enable_in_insert = false },
			symbol_in_winbar = { enable = false },
			rename = { in_select = false },
		},
	},
	{ "folke/trouble.nvim", keys = { { "<leader>lt", "<cmd>TroubleToggle<cr>", desc = "Trouble" } } },
	{ "tpope/vim-surround", lazy = true, events = "VeryLazy" },
	{
		"mhinz/vim-startify",
		keys = { { "<leader>;", "<cmd>Startify<cr>", desc = "Startify" } },
		lazy = false,
	},
	{ "tpope/vim-commentary", events = "VeryLazy" },
	{
		"akinsho/toggleterm.nvim",
		event = "VeryLazy",
		opts = { open_mapping = [[<c-\>]] },
	},
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		opts = {
			color_devicons = true,
		},
		keys = {
			{ "<leader>r", "<cmd>lua require('telescope.builtin').oldfiles()<CR>", desc = "Old Files" },
			{ "<leader><leader>", "<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>" },
			{
				"<C-B>",
				[[<cmd>lua require('telescope.builtin').buffers({ignore_current_buffer = true, sort_last_used = true})<CR>]],
			},
			{
				"<leader>sB",
				[[<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>]],
			},
			{

				"<leader>sd",
				[[<cmd>lua require('telescope.builtin').find_files({previewer = false})<CR>]],
			},
			{

				"<C-p>",
				[[<cmd>lua require('telescope.builtin').git_files({previewer = false})<CR>]],
			},
			{
				"<leader>f",
				[[<cmd>lua require('telescope.builtin').git_files({previewer = false})<CR>]],
			},
			{

				"<leader>sy",
				[[<cmd>lua require('telescope.builtin').registers()<CR>]],
			},
			{

				"<leader>sg",
				[[<cmd>lua require('telescope.builtin').live_grep()<CR>]],
			},
			{
				"<leader>ss",
				[[<cmd>lua require('telescope.builtin').builtin({include_extensions = true })<CR>]],
			},
			{
				"<leader><C-S>",
				[[<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>]],
			},
			{

				"<leader>s;",
				[[<cmd>lua require('telescope.builtin').command_history()<CR>]],
			},
			{
				"<leader>s.",
				[[<cmd>lua require('telescope.builtin').fd()<CR>]],
			},
			{

				"<leader>se",
				":lua require'telescope.builtin'.symbols{ sources = {'emoji'} }<CR>",
			},
		},
	},
	{
		"ruanyl/vim-gh-line",
		events = "BufRead",
	},
	{
		"folke/which-key.nvim",
		config = true,
	},
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-nvim-lsp",
		},
		events = "BufRead",
	},
	{
		"akinsho/bufferline.nvim",
		verylazy = true,
		keys = {
			{ "<TAB>", "<cmd>BufferLineCycleNext<cr>", desc = "Next Buffer" },
			{ "<S-TAB>", "<cmd>BufferLineCyclePrev<cr>", desc = "Prev Buffer" },
		},
		dependencies = "nvim-tree/nvim-web-devicons",
		config = true,
		event = "BufEnter",
	},
	{
		"nvim-lualine/lualine.nvim",
		config = true,
	},
	{
		"onsails/lspkind.nvim",
		events = "BufRead",
	},
	{
		-- Highlight, edit, and navigate code
		"nvim-treesitter/nvim-treesitter",
		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
		},
		events = "BufRead",
		config = function()
			require("nvim-treesitter.configs").setup({
				ensure_installed = {
					"c",
					"cpp",
					"go",
					"lua",
					"rust",
					"python",
					"tsx",
					"markdown",
					"markdown_inline",
					"typescript",
					"vim",
				},
				auto_install = false,
				highlight = { enable = true },
				indent = { enable = true, disable = { "python" } },
				incremental_selection = {
					enable = true,
					keymaps = {
						init_selection = "<c-space>",
						node_incremental = "<c-space>",
						scope_incremental = "<c-s>",
						node_decremental = "<M-space>",
					},
				},
				textobjects = {
					select = {
						enable = true,
						lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
						keymaps = {
							-- You can use the capture groups defined in textobjects.scm
							["aa"] = "@parameter.outer",
							["ia"] = "@parameter.inner",
							["af"] = "@function.outer",
							["if"] = "@function.inner",
							["ac"] = "@class.outer",
							["ic"] = "@class.inner",
						},
					},
					move = {
						enable = true,
						set_jumps = true, -- whether to set jumps in the jumplist
						goto_next_start = {
							["]m"] = "@function.outer",
							["]]"] = "@class.outer",
						},
						goto_next_end = {
							["]M"] = "@function.outer",
							["]["] = "@class.outer",
						},
						goto_previous_start = {
							["[m"] = "@function.outer",
							["[["] = "@class.outer",
						},
						goto_previous_end = {
							["[M"] = "@function.outer",
							["[]"] = "@class.outer",
						},
					},
					swap = {
						enable = true,
						swap_next = {
							["<leader>a"] = "@parameter.inner",
						},
						swap_previous = {
							["<leader>A"] = "@parameter.inner",
						},
					},
				},
			})
		end,
	},

	{
		"nvim-neo-tree/neo-tree.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
			"MunifTanjim/nui.nvim",
		},
		keys = {
			{ "<leader>e", "<cmd>Neotree focus<CR>", desc = "File explorer" },
		},
		opts = {
			close_if_last_window = true,
			window = {
				width = 20,
			},
		},
	},
	{
		"ntpeters/vim-better-whitespace",
		config = function()
			vim.g.better_whitespace_enabled = 1
			vim.g.strip_only_modified_lines = 1
			vim.g.strip_whitespace_confirm = 0
			vim.api.nvim_exec([[autocmd TermEnter * DisableWhitespace]], false)
		end,
		keys = {
			{ "<leader>S", "<cmd>StripWhitespace<cr>", desc = "Strip whitespaces" },
		},
	},
	{
		-- LSP Configuration & Plugins
		"neovim/nvim-lspconfig",
		dependencies = {
			"williamboman/mason.nvim",
			"williamboman/mason-lspconfig.nvim",
		},
		events = "BufRead",
		config = function()
			require("lspconfig").lua_ls.setup({
				settings = {
					Lua = {
						diagnostics = {
							-- Get the language server to recognize the `vim` global
							globals = { "vim" },
						},
					},
				},
			})
			local on_attach = function(_, bufnr)
				-- Create a command `:Format` local to the LSP buffer
				vim.api.nvim_buf_create_user_command(bufnr, "Format", function(_)
					vim.lsp.buf.format()
				end, { desc = "Format current buffer with LSP" })
			end
			local servers = {
				gopls = {
					analyses = {
						nilness = true,
						unusedparams = true,
						unusedwrite = true,
						useany = true,
					},
					experimentalPostfixCompletions = true,
					gofumpt = true,
					staticcheck = true,
					usePlaceholders = true,
				},
				rust_analyzer = {},
				pyright = {},
			}

			require("mason").setup()

			-- Ensure the servers above are installed
			local mason_lspconfig = require("mason-lspconfig")

			mason_lspconfig.setup({
				ensure_installed = vim.tbl_keys(servers),
			})

			mason_lspconfig.setup_handlers({
				function(server_name)
					require("lspconfig")[server_name].setup({
						capabilities = capabilities,
						on_attach = on_attach,
						settings = servers[server_name],
					})
				end,
			})
		end,
	},
	{
		"jose-elias-alvarez/null-ls.nvim",
		events = "BufRead",
		config = function()
			-- Null LS setup (part of LSP block)
			local null_ls = require("null-ls")

			null_ls.setup({
				sources = {
					null_ls.builtins.formatting.stylua,
					null_ls.builtins.formatting.autopep8,
					null_ls.builtins.diagnostics.eslint,
					null_ls.builtins.completion.spell,
				},
			})
		end,
	},
	{
		"lmburns/lf.nvim",
		cmd = "Lf",
		dependencies = { "nvim-lua/plenary.nvim", "akinsho/toggleterm.nvim" },
		opts = {
			winblend = 0,
			highlights = { NormalFloat = { guibg = "NONE" } },
			border = "single",
			escape_quit = false,
			height = vim.fn.float2nr(vim.fn.round(0.90 * vim.o.lines)), -- height of the *floating* window
			width = vim.fn.float2nr(vim.fn.round(0.90 * vim.o.columns)), -- height of the *floating* window
			default_file_manager = true,
		},
		keys = {
			{ "<leader>fl", "<cmd>Lf<cr>", desc = "LF File manager" },
			{ "<leader>e", "<cmd>Lf<cr>", desc = "LF File manager" },
		},
	},
	{
		"hrsh7th/nvim-cmp",
		dependencies = { "hrsh7th/cmp-nvim-lsp", "L3MON4D3/LuaSnip", "saadparwaiz1/cmp_luasnip" },
		events = "BufRead",
		config = function()
			-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
			local capabilities = vim.lsp.protocol.make_client_capabilities()
			capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

			-- nvim-cmp setup
			local cmp = require("cmp")
			local luasnip = require("luasnip")
			local lspkind = require("lspkind")

			luasnip.config.setup({})

			cmp.setup({
				formatting = {
					format = lspkind.cmp_format({
						mode = "symbol",
						maxwidth = 50,
						ellipsis_char = "...",
					}),
				},
				snippet = {
					expand = function(args)
						luasnip.lsp_expand(args.body)
					end,
				},
				mapping = cmp.mapping.preset.insert({
					["<C-d>"] = cmp.mapping.scroll_docs(-4),
					["<C-f>"] = cmp.mapping.scroll_docs(4),
					["<C-Space>"] = cmp.mapping.complete({}),
					["<CR>"] = cmp.mapping.confirm({
						behavior = cmp.ConfirmBehavior.Replace,
						select = true,
					}),
					["<Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif luasnip.expand_or_jumpable() then
							luasnip.expand_or_jump()
						else
							fallback()
						end
					end, { "i", "s" }),
					["<S-Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_prev_item()
						elseif luasnip.jumpable(-1) then
							luasnip.jump(-1)
						else
							fallback()
						end
					end, { "i", "s" }),
				}),
				sources = {
					{ name = "nvim_lsp" },
					{ name = "luasnip" },
					{ name = "path" },
					{ name = "buffer" },
				},
			})
		end,
	},
})

require("chmou.keymaps")
pcall(require, "local")
