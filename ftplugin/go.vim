setlocal tabstop=2
setlocal shiftwidth=2
set indentexpr=nvim_treesitter#indent()
setlocal autoread

nnoremap <buffer> <leader>lf <CMD>!goimports -w %<CR>
nnoremap <buffer> <leader>la <CMD>Lspsaga code_action<CR>
nnoremap <buffer> <leader>lo <CMD>Lspsaga outline<CR>
nnoremap <buffer> <leader>lR <CMD>Lspsaga rename ++project<CR>
nnoremap <buffer> <leader>lr <CMD>Lspsaga rename<CR>
nnoremap <buffer> <leader>ll <CMD>Lspsaga lsp_finder<CR>
nnoremap <buffer> ]e <CMD>Lspsaga diagnostic_jump_next<CR>
nnoremap <buffer> [e <CMD>Lspsaga diagnostic_jump_prev<CR>
nnoremap <buffer> K <cmd>Lspsaga hover_doc<CR>
