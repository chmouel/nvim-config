local map = vim.api.nvim_set_keymap
local default_opts = { noremap = true, silent = true }

map("n", "<leader>Lc", "<cmd>e ~/.config/nvim/init.lua<cr>", default_opts)
map("n", "<leader>Lr", "<cmd>luafile ~/.config/nvim/init.lua<cr>", default_opts)

map("i", "<A-b>", "<C-o>b", default_opts)
map("i", "<A-f>", "<C-o>e", default_opts)
map("i", "<C-a>", "<C-o>I", default_opts)
map("i", "<C-e>", "<C-o>A", default_opts)
map("i", "<C-b>", "<C-o>h", default_opts)
map("i", "<C-f>", "<C-o>l", default_opts)
map("i", "jj", "<ESC><CMD>w<CR>", default_opts)
map("i", "<C-s>", "<Esc>:w<CR>i", { noremap = true })

map("n", "<Leader>z", "<cmd>suspend<cr>", default_opts)
map("n", "<Leader>w", "<cmd>w<cr>", default_opts)
map("n", "<C-s>", "<cmd>w<cr>", default_opts)
map("n", "<leader>q", ":qa<CR>", default_opts)
map("n", "<leader>Q", ":wqa<CR>", default_opts)
map("n", "<C-q>", ":q<CR>", default_opts)

map("n", "<Leader>c", "<cmd>bd<cr>", default_opts)
--- terminal
map("t", "<ESC>", "<C-\\><C-n>", default_opts)
map("t", "<C-w><C-w>", "<C-\\><C-n><C-w><C-w><CR>", default_opts)

-- Emacs alike in command line
map("n", "<C-l>", "<C-^>", default_opts)
vim.cmd([[
cnoremap <C-A> <Home>
cnoremap <C-E> <End>
cnoremap <C-B> <Left>
cnoremap <C-F> <Right>
]])

map("n", "<leader><C-]>", "<cmd>ALENext<CR>", default_opts)
map("n", "<leader><C-[>", "<cmd>ALEPrevious<CR>", default_opts)
