vim.g.Hexokinase_highlighters = {'sign_column'}
vim.g.Hexokinase_optInPatterns = {
    'full_hex', 'rgb', 'rgba', 'hsl', 'hsla'
}
