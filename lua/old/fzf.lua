vim.api.nvim_set_keymap("n", "<leader>sf", ":Files<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>sb", ":Buffers<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>sg", ":GFiles<CR>", { noremap = true, silent = true })
